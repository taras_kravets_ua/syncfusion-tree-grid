import { Component, Input, ViewChild } from '@angular/core';
import { ChangeEventArgs, DropDownListComponent } from '@syncfusion/ej2-angular-dropdowns';
import { TreeGridComponent } from '@syncfusion/ej2-angular-treegrid';

@Component({
  selector: 'app-selection-menu',
  templateUrl: './selection-menu.component.html',
  styleUrls: ['./selection-menu.component.scss']
})
export class SelectionMenuComponent {
  @Input() treeGrid!: TreeGridComponent;
  fields1 = { text: 'type' , value: 'id'};
  fields2 = { text: 'mode' , value: 'id'};
  fields3 = { text: 'mode' , value: 'id'};
  d1data = [
    { id: 'Single', type: 'Single' },
    { id: 'Multiple', type: 'Multiple' },
  ];
  d2data = [
    { id: 'Row', mode: 'Row' },
    { id: 'Cell', mode: 'Cell' },
  ];
  d3data = [
    { id: 'Flow', mode: 'Flow' },
    // { id: 'Box', mode: 'Box' } Error appears therefore works wrong
  ];

  @ViewChild('dropdown1')
  private dropdown1!: DropDownListComponent;

  @ViewChild('dropdown2')
  private dropdown2!: DropDownListComponent;

  @ViewChild('dropdown3')
  private dropdown3!: DropDownListComponent;

  constructor() { }

  change1(e: ChangeEventArgs): void {
    const type: any = e.value;
    const mode: any = this.dropdown2.value;
    this.treeGrid.selectionSettings.type = type;
    if ( type === 'Multiple' && mode === 'Cell') {
      document.getElementById('cellselection')!.style.display = 'table-row';
    } else {
      document.getElementById('cellselection')!.style.display = 'none';
    }
  }

  change2(e: ChangeEventArgs): void {
    const mode: any = e.value;
    const type: any = this.dropdown1.value;
    this.treeGrid.selectionSettings.mode = mode;
    if ( type === 'Multiple' && mode === 'Cell' ) {
      document.getElementById('cellselection')!.style.display = 'table-row';
    } else {
      document.getElementById('cellselection')!.style.display = 'none';
    }
  }

  change3(e: ChangeEventArgs): void {
    this.treeGrid.selectionSettings.cellSelectionMode = e.value as any;
  }
}
