import { Component, Input } from '@angular/core';
import { TreeGridComponent } from '@syncfusion/ej2-angular-treegrid';
import { ChangeEventArgs } from '@syncfusion/ej2-angular-dropdowns';

@Component({
  selector: 'app-filter-menu',
  templateUrl: './filter-menu.component.html',
  styleUrls: ['./filter-menu.component.scss', '../selection-menu/selection-menu.component.scss']
})
export class FilterMenuComponent {
  @Input() treeGrid!: TreeGridComponent;
  ddlFields = { text: 'mode' , value: 'id'};
  typeFields = { text: 'mode' , value: 'id'};
  d1data = [
    { id: 'Parent', mode: 'Parent' },
    { id: 'Child', mode: 'Child' },
    { id: 'Both', mode: 'Both' },
    { id: 'None', mode: 'None' },
  ];
  d2data = [
    { id: 'Menu', mode: 'Menu' },
    { id: 'Excel', mode: 'Excel' },
    { id: 'FilterBar', mode: 'Filter Bar'}
  ];

  constructor() { }

  onChange(e: ChangeEventArgs): void {
    const mode = e.value;
    this.treeGrid.filterSettings.hierarchyMode = mode as any;
    this.treeGrid.clearFiltering();
  }

  change(e: ChangeEventArgs): void {
    const type = e.value;
    this.treeGrid.filterSettings.type = type as any;
    this.treeGrid.clearFiltering();
  }

}
