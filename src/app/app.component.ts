import { Component, OnInit } from '@angular/core';
import {
  FilterService,
  PageService,
  ResizeService,
  RowDDService,
  SelectionService,
  SortService,
  ReorderService,
  ToolbarService,
  EditService,
  ColumnChooserService
} from '@syncfusion/ej2-angular-treegrid';
import { FilterSettingsModel } from '@syncfusion/ej2-treegrid/src/treegrid/models/filter-settings-model';
import { SortSettingsModel } from '@syncfusion/ej2-treegrid/src/treegrid/models/sort-settings-model';
import { SelectionSettingsModel } from '@syncfusion/ej2-treegrid/src/treegrid/models/selection-settings-model';
import { EditSettingsModel } from '@syncfusion/ej2-treegrid/src/treegrid/models/edit-settings-model';
import {
  COLUMNS_SETTINGS,
  CrewInterface,
  EDIT_SETTINGS,
  FILTER_SETTINGS,
  getMockData,
  SELECTION_SETTINGS,
  SORT_SETTINGS,
  TOOLBAR_SETTINGS
} from './data';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
  providers: [
    PageService,
    SortService,
    FilterService,
    ResizeService,
    RowDDService,
    SelectionService,
    ReorderService,
    ToolbarService,
    EditService,
    ColumnChooserService
  ],
})
export class AppComponent implements OnInit {
  data: CrewInterface[] = getMockData();
  columnsSettings = COLUMNS_SETTINGS;
  sortSettings: SortSettingsModel = SORT_SETTINGS;
  filterSettings: FilterSettingsModel = FILTER_SETTINGS;
  toolbarSettings: string[] = TOOLBAR_SETTINGS;
  selectionSettings: SelectionSettingsModel = SELECTION_SETTINGS;
  editSettings: EditSettingsModel = EDIT_SETTINGS;

  constructor() {}

  ngOnInit(): void {
  }
}
