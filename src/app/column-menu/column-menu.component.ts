import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { Column, TreeGridComponent } from '@syncfusion/ej2-angular-treegrid';
import { ChangeEventArgs, DropDownListComponent } from '@syncfusion/ej2-angular-dropdowns';
import { ActionEventArgs } from '@syncfusion/ej2-grids';

@Component({
  selector: 'app-column-menu',
  templateUrl: './column-menu.component.html',
  styleUrls: ['./column-menu.component.scss', '../selection-menu/selection-menu.component.scss']
})
export class ColumnMenuComponent implements OnInit {
  @Input() treeGrid!: TreeGridComponent;
  @Input() columns!: any[];
  ddlfields = { text: 'name' , value: 'id' };
  d1data!: { id: string; name: string }[];
  d2data!: { id: string; name: string }[];
  fields = { text: 'name' , value: 'id' };
  backgroundColors = ['indianred', 'forestgreen', 'cyan', 'cornflowerblue'];
  textColor = ['red', 'black', 'white'];

  headerTitle = 'Name';

  @ViewChild('dropdown1')
  public dropdown1!: DropDownListComponent;
  @ViewChild('dropdown2')
  public dropdown2!: DropDownListComponent;

  constructor() { }

  ngOnInit(): void {
    this.d1data = this.buildD1Data(this.columns);
    this.d2data = this.buildD2Data(this.columns);
  }

  onChange(e: ChangeEventArgs): void {
    const columnName = e.value as string;
    this.headerTitle = this.d1data.find(el => el.id === e.value)?.name || '';
    const index: number = this.treeGrid.getColumnIndexByField(columnName);
    this.dropdown2.value = index.toString();
  }

  change(e: ChangeEventArgs): void {
    const columnName = this.dropdown1.value as string;
    const toColumnIndex: number = e.value as number;
    this.treeGrid.reorderColumns(
      columnName,
      ((this.treeGrid.columns[toColumnIndex]) as Column).field
    );
  }

  actionComplete(args: ActionEventArgs): void {
    if (args.requestType === 'reorder') {
      const columnName = this.dropdown1.value as string;
      const index = this.treeGrid.getColumnIndexByField(columnName);
      this.dropdown2.value = index.toString();
    }
  }

  setColumnHeader(): void {
    const columnName = this.dropdown1.value as string;
    const column = this.treeGrid.getColumnByField(columnName);
    this.d1data = this.d1data.map(el =>
      el.id === columnName ? {...el, name: this.headerTitle} : el
    );

    column.headerText = this.headerTitle;
    this.treeGrid.refreshColumns();
  }

  setClass(e: ChangeEventArgs): void {
    const columnName = this.dropdown1.value as string;
    const column = this.treeGrid.getColumnByField(columnName);
    column.customAttributes = {class: e.value};
    this.treeGrid.refreshColumns();
  }

  private buildD1Data(columns: any[]): {id: string; name: string}[] {
    return columns.reduce((acc, curr, i) => {
      acc.push({id: curr.field, name: curr.headerText});
      return acc;
    }, []);
  }

  private buildD2Data(columns: any[]): {id: string; name: string}[] {
    return columns.reduce((acc, curr, i) => {
      acc.push({id: `${i}`, name: `${i + 1}`});
      return acc;
    }, []);
  }
}
