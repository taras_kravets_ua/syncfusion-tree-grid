import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { TreeGridModule } from '@syncfusion/ej2-angular-treegrid';
import { DropDownListAllModule } from '@syncfusion/ej2-angular-dropdowns';
import { TextBoxModule } from '@syncfusion/ej2-angular-inputs';
import { ButtonModule } from '@syncfusion/ej2-angular-buttons';

import { AppComponent } from './app.component';
import { SelectionMenuComponent } from './selection-menu/selection-menu.component';
import { FilterMenuComponent } from './filter-menu/filter-menu.component';
import { ColumnMenuComponent } from './column-menu/column-menu.component';
import { FormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    AppComponent,
    SelectionMenuComponent,
    FilterMenuComponent,
    ColumnMenuComponent
  ],
  imports: [
    BrowserModule,
    TreeGridModule,
    DropDownListAllModule,
    TextBoxModule,
    ButtonModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
