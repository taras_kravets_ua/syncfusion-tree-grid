import { SortSettingsModel } from '@syncfusion/ej2-treegrid/src/treegrid/models/sort-settings-model';
import { FilterSettingsModel } from '@syncfusion/ej2-treegrid/src/treegrid/models/filter-settings-model';
import { SelectionSettingsModel } from '@syncfusion/ej2-treegrid/src/treegrid/models/selection-settings-model';
import { EditSettingsModel } from '@syncfusion/ej2-treegrid/src/treegrid/models/edit-settings-model';

type Player = {
  playerId: number;
  name: string;
  year: number;
  IQ: number;
  punchPower: number;
  height: number;
  salary: number;
  weight: number;
};

export interface CrewInterface extends Player {
  crew: Player[];
}

export const COLUMNS_SETTINGS = [
  {
    field: 'playerId',
    headerText: 'Player Id',
    width: 90,
    textAlign: 'Right',
    validationRules: {
      required: true,
      number: true
    }
  },
  {
    field: 'name',
    headerText: 'Name',
    width: 100,
    validationRules: {
      required: true
    }
  },
  {
    field: 'year',
    headerText: 'Year',
    width: 90,
    textAlign: 'Right',
    validationRules: {
      required: true,
      number: true,
      min: 1967,
      max: 2000
    }
  },
  {
    field: 'punchPower',
    headerText: 'Punch power',
    width: 120,
    textAlign: 'Right',
    validationRules: {
      required: true,
      number: true
    }
  },
  {
    field: 'height',
    headerText: 'Height',
    width: 80,
    textAlign: 'Right',
    validationRules: {
      required: true,
      number: true
    }
  },
  {
    field: 'weight',
    headerText: 'Weight',
    width: 80,
    textAlign: 'Right',
    validationRules: {
      required: true,
      number: true
    }
  },
  {
    field: 'IQ',
    headerText: 'IQ',
    width: 80,
    textAlign: 'Right',
    validationRules: {
      required: true,
      number: true
    }
  },
  {
    field: 'salary',
    headerText: 'Salary',
    width: 80,
    textAlign: 'Right',
    validationRules: {
      required: true,
      number: true
    }
  },
];

export const SORT_SETTINGS: SortSettingsModel = {
  columns: [
    { field: 'playerId', direction: 'Ascending'},
    { field: 'punchPower', direction: 'Ascending' },
    { field: 'IQ', direction: 'Ascending' },
  ]
};

export const FILTER_SETTINGS: FilterSettingsModel = {
  type: 'Excel',
  hierarchyMode: 'Parent',
  mode: 'Immediate'
};

export const TOOLBAR_SETTINGS: string[] = [
  'Add',
  'Edit',
  'Delete',
  'Update',
  'Cancel',
  'ColumnChooser'
];

export const SELECTION_SETTINGS: SelectionSettingsModel = { type: 'Multiple' };

export const EDIT_SETTINGS: EditSettingsModel = {
  allowEditing: true,
  allowAdding: true,
  allowDeleting: true,
  mode: 'Row',
  newRowPosition: 'Below'
};

export function getMockData(): CrewInterface[] {
  const mockData: CrewInterface[] = [];
  let parent = -1;
  const children = 'crew';
  const names = ['VINET', 'TOMSP', 'HANAR', 'VICTE', 'SUPRD', 'HANAR', 'CHOPS', 'RICSU', 'WELLI', 'HILAA', 'ERNSH', 'CENTC',
    'OTTIK', 'QUEDE', 'RATTC', 'ERNSH', 'FOLKO', 'BLONP', 'WARTH', 'FRANK', 'GROSR', 'WHITC', 'WARTH', 'SPLIR', 'RATTC', 'QUICK', 'VINET',
    'MAGAA', 'TORTU', 'MORGK', 'BERGS', 'LEHMS', 'BERGS', 'ROMEY', 'ROMEY', 'LILAS', 'LEHMS', 'QUICK', 'QUICK', 'RICAR', 'REGGC', 'BSBEV',
    'COMMI', 'QUEDE', 'TRADH', 'TORTU', 'RATTC', 'VINET', 'LILAS', 'BLONP', 'HUNGO', 'RICAR', 'MAGAA', 'WANDK', 'SUPRD', 'GODOS', 'TORTU',
    'OLDWO', 'ROMEY', 'LONEP', 'ANATR', 'HUNGO', 'THEBI', 'DUMON', 'WANDK', 'QUICK', 'RATTC', 'ISLAT', 'RATTC', 'LONEP', 'ISLAT', 'TORTU',
    'WARTH', 'ISLAT', 'PERIC', 'KOENE', 'SAVEA', 'KOENE', 'BOLID', 'FOLKO', 'FURIB', 'SPLIR', 'LILAS', 'BONAP', 'MEREP', 'WARTH', 'VICTE',
    'HUNGO', 'PRINI', 'FRANK', 'OLDWO', 'MEREP', 'BONAP', 'SIMOB', 'FRANK', 'LEHMS', 'WHITC', 'QUICK', 'RATTC', 'FAMIA'];
  for (let i = 0; i < 1000; i++) {
    if (i % 5 === 0) {
      parent = i;
    }
    if (i % 5 !== 0) {
      const num = isNaN((mockData.length % parent) - 1) ?  0 : (mockData.length % parent) - 1;
      mockData[num][children].push({
        playerId: i + 1,
        name: names[Math.floor(Math.random() * names.length)],
        year: 1967 + (i % 10),
        IQ: Math.floor(Math.random() * (100 - 70) + 70),
        punchPower: Math.floor(Math.random() * (300 - 150) + 150),
        height: Math.floor(Math.random() * (190 - 155) + 155),
        salary: Math.floor(Math.random() * (2000 - 1300) + 1300),
        weight: Math.floor(Math.random() * (90 - 65) + 65),
      });
    } else {
      mockData.push({
        playerId: i + 1,
        crew: [],
        name: names[Math.floor(Math.random() * names.length)],
        year: 1967 + (i % 10),
        IQ: Math.floor(Math.random() * (100 - 70) + 70),
        punchPower: Math.floor(Math.random() * (300 - 150) + 150),
        height: Math.floor(Math.random() * (190 - 155) + 155),
        salary: Math.floor(Math.random() * (2000 - 1000) + 1000),
        weight: Math.floor(Math.random() * (90 - 65) + 65),
      });
    }
  }
  return mockData;
}

